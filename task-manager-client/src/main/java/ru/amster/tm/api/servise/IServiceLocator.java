package ru.amster.tm.api.servise;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.endpoint.*;

public interface IServiceLocator {

    void setSession(String session);

    @NotNull String getSession();

    void setCommandService(ICommandService commandService);

    @NotNull
    AdminUserEndpoint getAdminUserEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    SessionEndpoint getSessionEndpoint();

    @NotNull
    ProjectEndpoint getProjectEndpoint();

    @NotNull
    TaskEndpoint getTaskEndpoint();

    @NotNull
    UserEndpoint getUserEndpoint();
}