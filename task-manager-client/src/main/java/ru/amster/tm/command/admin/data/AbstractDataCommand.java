package ru.amster.tm.command.admin.data;

import lombok.NoArgsConstructor;
import ru.amster.tm.command.AbstractCommand;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    protected static final String FILE_BINARY = "./data.bin";

    protected static final String FILE_XML = "./data.xml";

    protected static final String FILE_JSON = "./data.json";

    protected static final String FILE_BASE64 = "./data.base64";

}