package ru.amster.tm.command.admin.data.xml;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public final class DataXmlClearCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-xml-clear";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Remove xml data";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML CLEAR]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_XML);
        Files.delete(file.toPath());
        System.out.println("[OK]");
    }

}