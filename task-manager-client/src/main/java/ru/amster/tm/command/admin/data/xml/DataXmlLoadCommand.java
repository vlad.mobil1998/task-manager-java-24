package ru.amster.tm.command.admin.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.admin.data.AbstractDataCommand;
import ru.amster.tm.endpoint.AdminUserEndpoint;
import ru.amster.tm.endpoint.Domain;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public final class DataXmlLoadCommand extends AbstractDataCommand {

    @Override
    @NotNull
    public String name() {
        return "data-xml-load";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Load xml data to file";
    }

    @Override
    public void execute() throws IOException, ClassNotFoundException {
        System.out.println("[DATA XML LOAD]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        @NotNull final File file = new File(FILE_XML);
        @NotNull final FileInputStream fileInputStream = new FileInputStream(file);

        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);

        @NotNull final AdminUserEndpoint adminUserEndpoint = webServiceLocator.getAdminUserEndpoint();
        adminUserEndpoint.load(webServiceLocator.getSession(), domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

}