package ru.amster.tm.command.authentication;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.Role;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.exception.empty.EmptyLoginException;
import ru.amster.tm.exception.empty.EmptyPasswordException;
import ru.amster.tm.util.TerminalUtil;

public final class RegistrationCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "reg";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Register now";
    }

    @Override
    public void execute() {
        System.out.println("[REGISTRATION]");

        System.out.println("[ENTER LOGIN]");
        @Nullable final String login = TerminalUtil.nextLine();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();

        System.out.println("[ENTER PASSWORD]");
        @Nullable final String password = TerminalUtil.nextLine();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();

        System.out.println("[ENTER EMAIL]");
        @Nullable final String email = TerminalUtil.nextLine();
        @NotNull final UserEndpoint userEndpoint = webServiceLocator.getUserEndpoint();
        userEndpoint.createWithFourParamUser(login, password, email, Role.USER);

        System.out.println("[OK]");
    }

}