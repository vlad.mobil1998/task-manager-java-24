package ru.amster.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.ProjectEndpoint;
import ru.amster.tm.endpoint.Project;
import ru.amster.tm.exception.empty.EmptyProjectException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.ProjectUtil;
import ru.amster.tm.util.TerminalUtil;

public final class ProjectShowByIndexCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "project-v-i";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Show project by index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();

        System.out.println("ENTER INDEX");
        @NotNull final ProjectEndpoint projectEndpoint = webServiceLocator.getProjectEndpoint();
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Integer maxIndex = projectEndpoint.numberOfAllProjects(
                webServiceLocator.getSession()
        );
        if (index >= maxIndex) throw new InvalidIndexException(index, maxIndex);

        @Nullable final Project project = projectEndpoint.findProjectByIndex(
                webServiceLocator.getSession(),
                index
        );
        if (project == null) throw new EmptyProjectException();
        ProjectUtil.showProject(project);
        System.out.println("[OK]");
    }

}