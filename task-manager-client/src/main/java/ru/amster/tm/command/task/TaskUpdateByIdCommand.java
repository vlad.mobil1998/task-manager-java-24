package ru.amster.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.command.AbstractCommand;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.exception.empty.EmptyIdException;
import ru.amster.tm.exception.empty.EmptyNameException;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractCommand {

    @Override
    @NotNull
    public String name() {
        return "task-upd-id";
    }

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String description() {
        return " - Update task by id";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK]");
        if (webServiceLocator.getSession() == null) throw new AccessDeniedException();
        System.out.println("ENTER ID");
        @Nullable final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) throw new EmptyIdException();

        @NotNull final TaskEndpoint taskEndpoint = webServiceLocator.getTaskEndpoint();
        @Nullable final Task task = taskEndpoint.findTaskById(webServiceLocator.getSession(), id);
        if (task == null) throw new EmptyTaskException();

        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) throw new EmptyNameException();

        System.out.println("ENTER DESCRIPTION");
        @Nullable final String description = TerminalUtil.nextLine();
        @Nullable final Task taskUpdate =
                taskEndpoint.updateTaskById(webServiceLocator.getSession(), id, name, description);
        if (taskUpdate == null) throw new EmptyTaskException();
        System.out.println("[OK]");
    }

}