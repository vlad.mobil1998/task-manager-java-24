package ru.amster.tm.exception.empty;

import ru.amster.tm.exception.AbstractException;

public final class EmptyLastNameException extends AbstractException {

    public EmptyLastNameException() {
        super("Error! Last name is empty...");
    }

}