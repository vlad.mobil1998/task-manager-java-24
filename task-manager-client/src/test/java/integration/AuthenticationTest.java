package integration;

import marker.IntegrationTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.service.WebServiceLocator;

public class AuthenticationTest {

    WebServiceLocator webServiceLocator;

    @Before
    public void init() {
        webServiceLocator = new WebServiceLocator();
    }

    @Test
    @Category(IntegrationTest.class)
    public void login() {
        SessionEndpoint sessionEndpoint = webServiceLocator.getSessionEndpoint();
        String session = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(session);
    }



}
