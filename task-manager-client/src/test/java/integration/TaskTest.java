package integration;

import marker.IntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.Task;
import ru.amster.tm.endpoint.TaskEndpoint;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.service.WebServiceLocator;

@Category(IntegrationTest.class)
public class TaskTest {

    private static TaskEndpoint taskEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static String session, session1;

    @BeforeClass
    public static void init() {
        WebServiceLocator webServiceLocator = new WebServiceLocator();
        taskEndpoint = webServiceLocator.getTaskEndpoint();
        sessionEndpoint = webServiceLocator.getSessionEndpoint();

        session = sessionEndpoint.openSession("test", "test");
        session1 = sessionEndpoint.openSession("admin", "admin");

        taskEndpoint.createWithTwoParamTask(session, "demo");
        taskEndpoint.createWithTwoParamTask(session, "test");
        taskEndpoint.createWithTwoParamTask(session, "temp");

        taskEndpoint.createWithThreeParamTask(session1, "demo", "session1");
    }

    @Test
    @Category(IntegrationTest.class)
    public void findAndRemoveTaskTest() {
        Task task = taskEndpoint.findTaskByName(session, "demo");
        Assert.assertEquals("demo", task.getName());
        Assert.assertNotEquals("session1", task.getDescription());

        Task task1 = taskEndpoint.findTaskByName(session, "test");
        Assert.assertEquals(task1.getName(), "test");

        Task task3 = taskEndpoint.findTaskByIndex(session, 0);
        Assert.assertEquals("demo", task3.getName());
        Assert.assertNotEquals("session1", task3.getDescription());

        taskEndpoint.removeTaskByName(session, "demo");
        Task task5 = taskEndpoint.findTaskByName(session1, "demo");
        Assert.assertEquals("demo", task5.getName());
        Assert.assertEquals("session1", task5.getDescription());
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskException() {
        taskEndpoint.createWithTwoParamTask("adsedqwd2132dad/sadasda/sdas89yhuhqd", "ds");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskException() {
        taskEndpoint.findTaskByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void findTaskByIndexTaskException() {
        taskEndpoint.findTaskByIndex("adsedqwd2132dad/sadasda/sdas89yhuhqd", 0);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskException() {
        taskEndpoint.removeTaskByName("adsedqwd2132dad/sadasda/sdas89yhuhqd", "demo");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByIndexTaskException() {
        taskEndpoint.removeTaskByIndex("adsedqwd2132dad/sadasda/sdas89yhuhqd", 0);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskNullException() {
        taskEndpoint.createWithTwoParamTask(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskNullException() {
        taskEndpoint.findTaskByName(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void findTaskByIndexTaskNullException() {
        taskEndpoint.findTaskByIndex(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskNullException() {
        taskEndpoint.removeTaskByName(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByIndexTaskNullException() {
        taskEndpoint.removeTaskByIndex(session, null);
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskCreateTaskEmptyException() {
        taskEndpoint.createWithTwoParamTask(session, "");
    }
    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskShowByNameTaskEmptyException() {
        taskEndpoint.findTaskByName(session, "");
    }

    @Test(expected = Exception.class)
    @Category(IntegrationTest.class)
    public void taskRemoveByNameTaskEmptyException() {
        taskEndpoint.removeTaskByName(session, "");
    }

    @AfterClass
    public static void exit() {
        taskEndpoint.removeTaskByName(session, "test");
        taskEndpoint.removeTaskByName(session, "temp");
        taskEndpoint.removeTaskByName(session1, "demo");
        sessionEndpoint.closeSession(session1);
        sessionEndpoint.closeSession(session);
    }

}