package integration;

import marker.IntegrationTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.endpoint.Role;
import ru.amster.tm.endpoint.SessionEndpoint;
import ru.amster.tm.endpoint.User;
import ru.amster.tm.endpoint.UserEndpoint;
import ru.amster.tm.service.WebServiceLocator;

@Category(IntegrationTest.class)
public class UserTest {

    private static WebServiceLocator webServiceLocator;

    private static UserEndpoint userEndpoint;

    private static SessionEndpoint sessionEndpoint;

    private static String session, session1;

    @BeforeClass
    public static void init() {
        webServiceLocator = new WebServiceLocator();
        userEndpoint = webServiceLocator.getUserEndpoint();
        sessionEndpoint = webServiceLocator.getSessionEndpoint();
        session = sessionEndpoint.openSession("test", "test");
        session1 = sessionEndpoint.openSession("admin", "admin");
    }

    @Test
    public void findUser() {
        User user = userEndpoint.findUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals("test", user.getLogin());

        User user1 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user1);
        Assert.assertEquals("admin", user1.getLogin());
    }

    @Test
    public void userUpdateProfile() {
        User user = userEndpoint.updateUserEmail(session, "test@test.com");
        User user1 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user);
        Assert.assertNotNull(user1);
        Assert.assertEquals(user.getEmail(), "test@test.com");
        Assert.assertNotEquals(user1.getEmail(), "test@test.com");

        User user2 = userEndpoint.updateUserFirstName(session1, "tester");
        User user3 = userEndpoint.findUser(session);
        Assert.assertNotNull(user2);
        Assert.assertNotNull(user3);
        Assert.assertEquals(user2.getFistName(), "tester");
        Assert.assertNotEquals(user3.getFistName(), "tester");

        User user4 = userEndpoint.updateUserMiddleName(session, "testim");
        User user5 = userEndpoint.findUser(session1);
        Assert.assertNotNull(user4);
        Assert.assertNotNull(user5);
        Assert.assertEquals(user4.getMiddleName(), "testim");
        Assert.assertNotEquals(user5.getMiddleName(), "testim");

        User user6 = userEndpoint.updateUserLastName(session1, "testLast");
        User user7 = userEndpoint.findUser(session);
        Assert.assertNotNull(user6);
        Assert.assertNotNull(user7);
        Assert.assertEquals(user6.getLastName(), "testLast");
        Assert.assertNotEquals(user7.getLastName(), "testLast");
    }

    @Test(expected = RuntimeException.class)
    public void userCreateAndRemoveTest() {
        userEndpoint.createWithThreeParamUser("temp", "temp", Role.USER);

        String session = sessionEndpoint.openSession("temp", "temp");
        User user = userEndpoint.updateUserPassword(session, "temp1");
        Assert.assertNotNull(user);
        sessionEndpoint.closeSession(session);
        session = null;
        session = sessionEndpoint.openSession("temp", "temp1");
        Assert.assertNotNull(session);

        userEndpoint.removeUser(session);
        String session1 = sessionEndpoint.openSession("temp", "temp");
        Assert.assertNull(session1);
    }

    @AfterClass
    public static void exit() {
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(session1);
    }

}