package ru.amster.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void merge(@NotNull Object record);

    void removeAll();

    List<E> getEntity();

    void remove(@NotNull Object record);

    Boolean contains(@NotNull String id);

}