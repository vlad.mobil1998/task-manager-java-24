package ru.amster.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.service.ServiceLocator;

import javax.xml.ws.Endpoint;

public final class Bootstrap {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private void initProperty() throws Exception {
        serviceLocator.getPropertyServer().init();
    }

    private void initUsers() {
        serviceLocator.getUserService().create("test", "test", "test@test.ru");
        serviceLocator.getUserService().create("test1", "test1", "test@test.ru");
        serviceLocator.getUserService().create("admin", "admin", Role.ADMIN);
    }

    private void initEndpoint() {
        registration(serviceLocator.getProjectEndpoint());
        registration(serviceLocator.getTaskEndpoint());
        registration(serviceLocator.getUserEndpoint());
        registration(serviceLocator.getAdminUserEndpoint());
        registration(serviceLocator.getSessionEndpoint());
    }

    private void registration(final Object endpoint) {
        @NotNull final String host = serviceLocator.getPropertyServer().getServerHost();
        @NotNull final Integer port = serviceLocator.getPropertyServer().getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?WSDL";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }


    public final void run(@NotNull final String[] args) throws Exception {
        initProperty();
        initUsers();
        initEndpoint();
    }

}