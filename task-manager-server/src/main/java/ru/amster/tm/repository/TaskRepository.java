package ru.amster.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.empty.EmptyTaskException;
import ru.amster.tm.exception.system.InvalidIndexException;
import ru.amster.tm.exception.user.AccessDeniedException;

import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final List<Task> result = new ArrayList<>();
        for (@NotNull final Task task : records) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        for (@NotNull final Task task : records) {
            if (!userId.equals(task.getUserId())) continue;
            remove(task);
            return;
        }
    }

    @NotNull
    @Override
    public Task findOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Task result = null;
        for (@NotNull final Task task : records) {
            if (!name.equals(task.getName()) || !userId.equals(task.getUserId())) continue;
            result = task;
            break;
        }
        if (result == null) throw new EmptyTaskException();
        return result;
    }

    @NotNull
    @Override
    public Task findOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        if (records.size() <= index) throw new InvalidIndexException(index, records.size());
        @Nullable final Task task = records.get(index);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        return task;
    }

    @NotNull
    @Override
    public Task findOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable Task result = null;
        for (@NotNull final Task task : records) {
            if (!id.equals(task.getId())) continue;
            result = task;
            break;
        }
        if (result == null) throw new EmptyTaskException();
        if (!userId.equals(result.getUserId())) throw new AccessDeniedException();
        return result;
    }

    @NotNull
    @Override
    public Task removeOneByName(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Task task = findOneByName(userId, name);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(task);
        return task;
    }

    @NotNull
    @Override
    public Task removeOneByIndex(@Nullable final String userId, @NotNull final Integer index) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Task task = findOneByIndex(userId, index);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(task);
        return task;
    }

    @NotNull
    @Override
    public Task removeOneById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final Task task = findOneById(userId, id);
        if (task == null) throw new EmptyTaskException();
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        remove(task);
        return task;
    }

    @NotNull
    @Override
    public Integer numberOfAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @NotNull final List<Task> result = findAll(userId);
        return result.size();
    }

}