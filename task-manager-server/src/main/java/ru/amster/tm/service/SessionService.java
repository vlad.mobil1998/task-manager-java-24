package ru.amster.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.amster.tm.api.repository.ISessionRepository;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.empty.EmptyEntityException;
import ru.amster.tm.exception.empty.EmptySignatureException;
import ru.amster.tm.exception.empty.EmptyUserException;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.util.EncryptUtil;
import ru.amster.tm.util.HashUtil;
import ru.amster.tm.util.SignatureUtil;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private IServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(@NotNull final ISessionRepository sessionRepository) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
    }

    @Override
    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public void close(@NotNull final String sessionSecret) {
        Session session = validate(sessionSecret);
        sessionRepository.removeByUserId(session.getUserId());
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @Nullable final String signature = SignatureUtil.sign(session);
        if (signature == null) throw new EmptySignatureException();
        session.setSignature(signature);
        return session;
    }

    @Override
    @NotNull
    public Session validate(@Nullable final String sessionSecret) {
        if (sessionSecret == null || sessionSecret.isEmpty()) throw new  AccessDeniedException();
        Session session = null;
        try {
            session = decryptSession(sessionSecret);
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }

        if (session == null) throw new AccessDeniedException();

        if (session.getSignature() == null
                || session.getSignature().isEmpty()) throw new AccessDeniedException();

        if (session.getUserId() == null
                || session.getUserId().isEmpty()) throw new AccessDeniedException();

        if (session.getTimestamp() == null) throw new AccessDeniedException();

        if(System.currentTimeMillis() - session.getTimestamp() > 300000) {
            signOutByUserId(session.getUserId());
            throw new AccessDeniedException();
        }

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable User user = userService.findById(session.getUserId());
        if (user == null) throw new EmptyUserException();
        if (user.getLocked()) throw new AccessDeniedException();
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();

        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(sessionTemp).getSignature();
        @NotNull final Boolean chek = signatureSource.equals(signatureTarget);
        if (!chek) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public Session validate(@NotNull final String sessionSecret, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        @Nullable Session session = null;
        try {
            session = validate(sessionSecret);
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }
        @NotNull final String userId = session.getUserId();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.findById(userId);

        if (user.getRole() == null) throw new AccessDeniedException();
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || login.isEmpty()) return false;

        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return false;

        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Nullable
    @Override
    public String open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;

        IUserService userService = serviceLocator.getUserService();
        @NotNull final User user = userService.findByLogin(login);
        if (user.getLocked()) throw new AccessDeniedException();

        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.merge(session);
        try {
            return cryptSession(sign(session));
        } catch (Exception e) {
            System.out.println(e.fillInStackTrace());
        }
        return null;
    }

    private String cryptSession(@NotNull final Session session) throws  Exception {
        @NotNull final String key = serviceLocator.getPropertyServer().getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return EncryptUtil.encrypt(json, key);
    }

    private Session decryptSession(@Nullable final String cryptSession) throws Exception {
        if (cryptSession == null || cryptSession.isEmpty()) throw new EmptyEntityException();
        @NotNull final String key = serviceLocator.getPropertyServer().getSecretKey();
        @NotNull final String json = EncryptUtil.decrypt(cryptSession, key);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        return session;
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return;
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) return;
        @NotNull final String userId = user.getId();
        sessionRepository.removeByUserId(userId);
    }

    @Override
    public void signOutByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        sessionRepository.removeByUserId(userId);
    }

}