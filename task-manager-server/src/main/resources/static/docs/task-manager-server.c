#include <enunciate-common.c>
#ifndef DEF_task_manager_server_ns1_domain_H
#define DEF_task_manager_server_ns1_domain_H

/**
 * (no documentation provided)
 */
struct task_manager_server_ns1_domain {


  /**
   * (no documentation provided)
   */
  struct task_manager_server_ns1_project *projects;

  /**
   * Size of the projects array.
   */
  int _sizeof_projects;

  /**
   * (no documentation provided)
   */
  struct task_manager_server_ns1_task *tasks;

  /**
   * Size of the tasks array.
   */
  int _sizeof_tasks;

  /**
   * (no documentation provided)
   */
  struct task_manager_server_ns1_user *users;

  /**
   * Size of the users array.
   */
  int _sizeof_users;
};

/**
 * Reads a Domain from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Domain, or NULL in case of error.
 */
static struct task_manager_server_ns1_domain *xmlTextReaderReadNs1DomainType(xmlTextReaderPtr reader);

/**
 * Writes a Domain to XML.
 *
 * @param writer The XML writer.
 * @param _domain The Domain to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1DomainType(xmlTextWriterPtr writer, struct task_manager_server_ns1_domain *_domain);

/**
 * Frees the elements of a Domain.
 *
 * @param _domain The Domain to free.
 */
static void freeNs1DomainType(struct task_manager_server_ns1_domain *_domain);

#endif /* DEF_task_manager_server_ns1_domain_H */
#ifndef DEF_task_manager_server_ns1_role_H
#define DEF_task_manager_server_ns1_role_H

/**
 * (no documentation provided)
 */
enum task_manager_server_ns1_role {

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_SERVER_NS1_ROLE_USER,

  /**
   * (no documentation provided)
   */
  TASK_MANAGER_SERVER_NS1_ROLE_ADMIN
};

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum task_manager_server_ns1_role *xmlTextReaderReadNs1RoleType(xmlTextReaderPtr reader);

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1RoleType(xmlTextWriterPtr writer, enum task_manager_server_ns1_role *_role);

/**
 * Frees a Role.
 *
 * @param _role The Role to free.
 */
static void freeNs1RoleType(enum task_manager_server_ns1_role *_role);

#endif
#ifndef DEF_task_manager_server_ns1_project_H
#define DEF_task_manager_server_ns1_project_H

/**
 * (no documentation provided)
 */
struct task_manager_server_ns1_project {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *name;

  /**
   * (no documentation provided)
   */
  xmlChar *description;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;
};

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Project, or NULL in case of error.
 */
static struct task_manager_server_ns1_project *xmlTextReaderReadNs1ProjectType(xmlTextReaderPtr reader);

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1ProjectType(xmlTextWriterPtr writer, struct task_manager_server_ns1_project *_project);

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs1ProjectType(struct task_manager_server_ns1_project *_project);

#endif /* DEF_task_manager_server_ns1_project_H */
#ifndef DEF_task_manager_server_ns1_session_H
#define DEF_task_manager_server_ns1_session_H

/**
 * (no documentation provided)
 */
struct task_manager_server_ns1_session {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  long long *timestamp;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;

  /**
   * (no documentation provided)
   */
  xmlChar *signature;
};

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Session, or NULL in case of error.
 */
static struct task_manager_server_ns1_session *xmlTextReaderReadNs1SessionType(xmlTextReaderPtr reader);

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1SessionType(xmlTextWriterPtr writer, struct task_manager_server_ns1_session *_session);

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs1SessionType(struct task_manager_server_ns1_session *_session);

#endif /* DEF_task_manager_server_ns1_session_H */
#ifndef DEF_task_manager_server_ns1_task_H
#define DEF_task_manager_server_ns1_task_H

/**
 * (no documentation provided)
 */
struct task_manager_server_ns1_task {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *name;

  /**
   * (no documentation provided)
   */
  xmlChar *description;

  /**
   * (no documentation provided)
   */
  xmlChar *userId;
};

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Task, or NULL in case of error.
 */
static struct task_manager_server_ns1_task *xmlTextReaderReadNs1TaskType(xmlTextReaderPtr reader);

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1TaskType(xmlTextWriterPtr writer, struct task_manager_server_ns1_task *_task);

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs1TaskType(struct task_manager_server_ns1_task *_task);

#endif /* DEF_task_manager_server_ns1_task_H */
#ifndef DEF_task_manager_server_ns1_user_H
#define DEF_task_manager_server_ns1_user_H

/**
 * (no documentation provided)
 */
struct task_manager_server_ns1_user {


  /**
   * (no documentation provided)
   */
  xmlChar *id;

  /**
   * (no documentation provided)
   */
  xmlChar *login;

  /**
   * (no documentation provided)
   */
  xmlChar *passwordHash;

  /**
   * (no documentation provided)
   */
  xmlChar *email;

  /**
   * (no documentation provided)
   */
  xmlChar *fistName;

  /**
   * (no documentation provided)
   */
  xmlChar *lastName;

  /**
   * (no documentation provided)
   */
  xmlChar *middleName;

  /**
   * (no documentation provided)
   */
  enum task_manager_server_ns1_role *role;

  /**
   * (no documentation provided)
   */
  int *locked;
};

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The User, or NULL in case of error.
 */
static struct task_manager_server_ns1_user *xmlTextReaderReadNs1UserType(xmlTextReaderPtr reader);

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1UserType(xmlTextWriterPtr writer, struct task_manager_server_ns1_user *_user);

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs1UserType(struct task_manager_server_ns1_user *_user);

#endif /* DEF_task_manager_server_ns1_user_H */
#ifndef DEF_task_manager_server_ns1_domain_M
#define DEF_task_manager_server_ns1_domain_M

/**
 * Reads a Domain from XML. The reader is assumed to be at the start element.
 *
 * @return the Domain, or NULL in case of error.
 */
static struct task_manager_server_ns1_domain *xmlTextReaderReadNs1DomainType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_server_ns1_domain *_domain = calloc(1, sizeof(struct task_manager_server_ns1_domain));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs1DomainType(_domain);
        free(_domain);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "projects", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}projects of type {}project.\n");
#endif
        _child_accessor = xmlTextReaderReadNs1ProjectType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}projects of type {}project.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->projects = realloc(_domain->projects, (_domain->_sizeof_projects + 1) * sizeof(struct task_manager_server_ns1_project));
        memcpy(&(_domain->projects[_domain->_sizeof_projects++]), _child_accessor, sizeof(struct task_manager_server_ns1_project));
        free(_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "tasks", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}tasks of type {}task.\n");
#endif
        _child_accessor = xmlTextReaderReadNs1TaskType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}tasks of type {}task.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->tasks = realloc(_domain->tasks, (_domain->_sizeof_tasks + 1) * sizeof(struct task_manager_server_ns1_task));
        memcpy(&(_domain->tasks[_domain->_sizeof_tasks++]), _child_accessor, sizeof(struct task_manager_server_ns1_task));
        free(_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "users", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}users of type {}user.\n");
#endif
        _child_accessor = xmlTextReaderReadNs1UserType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}users of type {}user.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1DomainType(_domain);
          free(_domain);
          return NULL;
        }

        _domain->users = realloc(_domain->users, (_domain->_sizeof_users + 1) * sizeof(struct task_manager_server_ns1_user));
        memcpy(&(_domain->users[_domain->_sizeof_users++]), _child_accessor, sizeof(struct task_manager_server_ns1_user));
        free(_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {}domain.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {}domain. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _domain;
}

/**
 * Writes a Domain to XML.
 *
 * @param writer The XML writer.
 * @param _domain The Domain to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1DomainType(xmlTextWriterPtr writer, struct task_manager_server_ns1_domain *_domain) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  for (i = 0; i < _domain->_sizeof_projects; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "projects", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {}project for element {}projects...\n");
#endif
    status = xmlTextWriterWriteNs1ProjectType(writer, &(_domain->projects[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {}project for element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}projects. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  for (i = 0; i < _domain->_sizeof_tasks; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "tasks", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {}task for element {}tasks...\n");
#endif
    status = xmlTextWriterWriteNs1TaskType(writer, &(_domain->tasks[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {}task for element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}tasks. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  for (i = 0; i < _domain->_sizeof_users; i++) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "users", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {}user for element {}users...\n");
#endif
    status = xmlTextWriterWriteNs1UserType(writer, &(_domain->users[i]));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {}user for element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}users. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Domain.
 *
 * @param _domain The Domain to free.
 */
static void freeNs1DomainType(struct task_manager_server_ns1_domain *_domain) {
  int i;
  if (_domain->projects != NULL) {
    for (i = 0; i < _domain->_sizeof_projects; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor projects[%i] of type task_manager_server_ns1_domain...\n", i);
#endif
      freeNs1ProjectType(&(_domain->projects[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor projects of type task_manager_server_ns1_domain...\n");
#endif
    free(_domain->projects);
  }
  if (_domain->tasks != NULL) {
    for (i = 0; i < _domain->_sizeof_tasks; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor tasks[%i] of type task_manager_server_ns1_domain...\n", i);
#endif
      freeNs1TaskType(&(_domain->tasks[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor tasks of type task_manager_server_ns1_domain...\n");
#endif
    free(_domain->tasks);
  }
  if (_domain->users != NULL) {
    for (i = 0; i < _domain->_sizeof_users; i++) {
#if DEBUG_ENUNCIATE > 1
      printf("Freeing accessor users[%i] of type task_manager_server_ns1_domain...\n", i);
#endif
      freeNs1UserType(&(_domain->users[i]));
    }
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor users of type task_manager_server_ns1_domain...\n");
#endif
    free(_domain->users);
  }
}
#endif /* DEF_task_manager_server_ns1_domain_M */
#ifndef DEF_task_manager_server_ns1_role_M
#define DEF_task_manager_server_ns1_role_M

/**
 * Reads a Role from XML. The reader is assumed to be at the start element.
 *
 * @param reader The XML reader.
 * @return The Role, or NULL if unable to be read.
 */
static enum task_manager_server_ns1_role *xmlTextReaderReadNs1RoleType(xmlTextReaderPtr reader) {
  xmlChar *enumValue = xmlTextReaderReadEntireNodeValue(reader);
  enum task_manager_server_ns1_role *value = calloc(1, sizeof(enum task_manager_server_ns1_role));
  if (enumValue != NULL) {
    if (xmlStrcmp(enumValue, BAD_CAST "USER") == 0) {
      *value = TASK_MANAGER_SERVER_NS1_ROLE_USER;
      free(enumValue);
      return value;
    }
    if (xmlStrcmp(enumValue, BAD_CAST "ADMIN") == 0) {
      *value = TASK_MANAGER_SERVER_NS1_ROLE_ADMIN;
      free(enumValue);
      return value;
    }
#if DEBUG_ENUNCIATE
    printf("Attempt to read enum value failed: %s doesn't match an enum value.\n", enumValue);
#endif
  }
#if DEBUG_ENUNCIATE
  else {
    printf("Attempt to read enum value failed: NULL value.\n");
  }
#endif

  return NULL;
}

/**
 * Writes a Role to XML.
 *
 * @param writer The XML writer.
 * @param _role The Role to write.
 * @return The bytes written (may be 0 in case of buffering) or -1 in case of error.
 */
static int xmlTextWriterWriteNs1RoleType(xmlTextWriterPtr writer, enum task_manager_server_ns1_role *_role) {
  switch (*_role) {
    case TASK_MANAGER_SERVER_NS1_ROLE_USER:
      return xmlTextWriterWriteString(writer, BAD_CAST "USER");
    case TASK_MANAGER_SERVER_NS1_ROLE_ADMIN:
      return xmlTextWriterWriteString(writer, BAD_CAST "ADMIN");
  }

#if DEBUG_ENUNCIATE
  printf("Unable to write enum value (no valid value found).\n");
#endif
  return -1;
}

/**
 * Frees a Role.
 *
 * @param _role The Role to free.
 */
static void freeNs1RoleType(enum task_manager_server_ns1_role *_role) {
  //no-op
}
#endif /* DEF_task_manager_server_ns1_role_M */
#ifndef DEF_task_manager_server_ns1_project_M
#define DEF_task_manager_server_ns1_project_M

/**
 * Reads a Project from XML. The reader is assumed to be at the start element.
 *
 * @return the Project, or NULL in case of error.
 */
static struct task_manager_server_ns1_project *xmlTextReaderReadNs1ProjectType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_server_ns1_project *_project = calloc(1, sizeof(struct task_manager_server_ns1_project));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs1ProjectType(_project);
        free(_project);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->name = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->description = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1ProjectType(_project);
          free(_project);
          return NULL;
        }

        _project->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {}project.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {}project. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _project;
}

/**
 * Writes a Project to XML.
 *
 * @param writer The XML writer.
 * @param _project The Project to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1ProjectType(xmlTextWriterPtr writer, struct task_manager_server_ns1_project *_project) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_project->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->name != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}name...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->name));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->description != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}description...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->description));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_project->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_project->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Project.
 *
 * @param _project The Project to free.
 */
static void freeNs1ProjectType(struct task_manager_server_ns1_project *_project) {
  int i;
  if (_project->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_server_ns1_project...\n");
#endif
    freeXsStringType(_project->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_server_ns1_project...\n");
#endif
    free(_project->id);
  }
  if (_project->name != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor name of type task_manager_server_ns1_project...\n");
#endif
    freeXsStringType(_project->name);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor name of type task_manager_server_ns1_project...\n");
#endif
    free(_project->name);
  }
  if (_project->description != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor description of type task_manager_server_ns1_project...\n");
#endif
    freeXsStringType(_project->description);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor description of type task_manager_server_ns1_project...\n");
#endif
    free(_project->description);
  }
  if (_project->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_server_ns1_project...\n");
#endif
    freeXsStringType(_project->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_server_ns1_project...\n");
#endif
    free(_project->userId);
  }
}
#endif /* DEF_task_manager_server_ns1_project_M */
#ifndef DEF_task_manager_server_ns1_session_M
#define DEF_task_manager_server_ns1_session_M

/**
 * Reads a Session from XML. The reader is assumed to be at the start element.
 *
 * @return the Session, or NULL in case of error.
 */
static struct task_manager_server_ns1_session *xmlTextReaderReadNs1SessionType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_server_ns1_session *_session = calloc(1, sizeof(struct task_manager_server_ns1_session));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs1SessionType(_session);
        free(_session);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "timestamp", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}timestamp of type {http://www.w3.org/2001/XMLSchema}long.\n");
#endif
        _child_accessor = xmlTextReaderReadXsLongType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}timestamp of type {http://www.w3.org/2001/XMLSchema}long.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->timestamp = ((long long*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "signature", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}signature of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1SessionType(_session);
          free(_session);
          return NULL;
        }

        _session->signature = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {}session.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {}session. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _session;
}

/**
 * Writes a Session to XML.
 *
 * @param writer The XML writer.
 * @param _session The Session to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1SessionType(xmlTextWriterPtr writer, struct task_manager_server_ns1_session *_session) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_session->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->timestamp != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "timestamp", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}long for element {}timestamp...\n");
#endif
    status = xmlTextWriterWriteXsLongType(writer, (_session->timestamp));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}long for element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}timestamp. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_session->signature != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "signature", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}signature...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_session->signature));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}signature. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Session.
 *
 * @param _session The Session to free.
 */
static void freeNs1SessionType(struct task_manager_server_ns1_session *_session) {
  int i;
  if (_session->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_server_ns1_session...\n");
#endif
    freeXsStringType(_session->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_server_ns1_session...\n");
#endif
    free(_session->id);
  }
  if (_session->timestamp != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor timestamp of type task_manager_server_ns1_session...\n");
#endif
    freeXsLongType(_session->timestamp);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor timestamp of type task_manager_server_ns1_session...\n");
#endif
    free(_session->timestamp);
  }
  if (_session->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_server_ns1_session...\n");
#endif
    freeXsStringType(_session->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_server_ns1_session...\n");
#endif
    free(_session->userId);
  }
  if (_session->signature != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor signature of type task_manager_server_ns1_session...\n");
#endif
    freeXsStringType(_session->signature);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor signature of type task_manager_server_ns1_session...\n");
#endif
    free(_session->signature);
  }
}
#endif /* DEF_task_manager_server_ns1_session_M */
#ifndef DEF_task_manager_server_ns1_task_M
#define DEF_task_manager_server_ns1_task_M

/**
 * Reads a Task from XML. The reader is assumed to be at the start element.
 *
 * @return the Task, or NULL in case of error.
 */
static struct task_manager_server_ns1_task *xmlTextReaderReadNs1TaskType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_server_ns1_task *_task = calloc(1, sizeof(struct task_manager_server_ns1_task));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs1TaskType(_task);
        free(_task);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "name", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}name of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->name = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "description", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}description of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->description = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "userId", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}userId of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1TaskType(_task);
          free(_task);
          return NULL;
        }

        _task->userId = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {}task.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {}task. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _task;
}

/**
 * Writes a Task to XML.
 *
 * @param writer The XML writer.
 * @param _task The Task to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1TaskType(xmlTextWriterPtr writer, struct task_manager_server_ns1_task *_task) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_task->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->name != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "name", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}name...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->name));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}name. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->description != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "description", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}description...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->description));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}description. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_task->userId != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "userId", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}userId...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_task->userId));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}userId. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a Task.
 *
 * @param _task The Task to free.
 */
static void freeNs1TaskType(struct task_manager_server_ns1_task *_task) {
  int i;
  if (_task->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_server_ns1_task...\n");
#endif
    freeXsStringType(_task->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_server_ns1_task...\n");
#endif
    free(_task->id);
  }
  if (_task->name != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor name of type task_manager_server_ns1_task...\n");
#endif
    freeXsStringType(_task->name);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor name of type task_manager_server_ns1_task...\n");
#endif
    free(_task->name);
  }
  if (_task->description != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor description of type task_manager_server_ns1_task...\n");
#endif
    freeXsStringType(_task->description);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor description of type task_manager_server_ns1_task...\n");
#endif
    free(_task->description);
  }
  if (_task->userId != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor userId of type task_manager_server_ns1_task...\n");
#endif
    freeXsStringType(_task->userId);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor userId of type task_manager_server_ns1_task...\n");
#endif
    free(_task->userId);
  }
}
#endif /* DEF_task_manager_server_ns1_task_M */
#ifndef DEF_task_manager_server_ns1_user_M
#define DEF_task_manager_server_ns1_user_M

/**
 * Reads a User from XML. The reader is assumed to be at the start element.
 *
 * @return the User, or NULL in case of error.
 */
static struct task_manager_server_ns1_user *xmlTextReaderReadNs1UserType(xmlTextReaderPtr reader) {
  int status, depth;
  void *_child_accessor;
  struct task_manager_server_ns1_user *_user = calloc(1, sizeof(struct task_manager_server_ns1_user));



  if (xmlTextReaderIsEmptyElement(reader) == 0) {
    depth = xmlTextReaderDepth(reader);//track the depth.
    status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);

    while (xmlTextReaderDepth(reader) > depth) {
      if (status < 1) {
        //panic: XML read error.
#if DEBUG_ENUNCIATE
        printf("Failure to advance to next child element.\n");
#endif
        freeNs1UserType(_user);
        free(_user);
        return NULL;
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "id", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}id of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->id = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "login", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}login of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->login = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "passwordHash", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}passwordHash of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->passwordHash = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "email", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}email of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->email = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "fistName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}fistName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->fistName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "lastName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}lastName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->lastName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "middleName", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
        _child_accessor = xmlTextReaderReadXsStringType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}middleName of type {http://www.w3.org/2001/XMLSchema}string.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->middleName = ((xmlChar*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "role", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}role of type {}role.\n");
#endif
        _child_accessor = xmlTextReaderReadNs1RoleType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}role of type {}role.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->role = ((enum task_manager_server_ns1_role*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else if (xmlTextReaderNodeType(reader) == XML_READER_TYPE_ELEMENT
        && xmlStrcmp(BAD_CAST "locked", xmlTextReaderConstLocalName(reader)) == 0
        && xmlTextReaderConstNamespaceUri(reader) == NULL) {

#if DEBUG_ENUNCIATE > 1
        printf("Attempting to read choice {}locked of type {http://www.w3.org/2001/XMLSchema}boolean.\n");
#endif
        _child_accessor = xmlTextReaderReadXsBooleanType(reader);
        if (_child_accessor == NULL) {
#if DEBUG_ENUNCIATE
          printf("Failed to read choice {}locked of type {http://www.w3.org/2001/XMLSchema}boolean.\n");
#endif
          //panic: unable to read the child element for some reason.
          freeNs1UserType(_user);
          free(_user);
          return NULL;
        }

        _user->locked = ((int*)_child_accessor);
        status = xmlTextReaderAdvanceToNextStartOrEndElement(reader);
      }
      else {
#if DEBUG_ENUNCIATE > 1
        if (xmlTextReaderConstNamespaceUri(reader) == NULL) {
          printf("unknown child element {}%s for type {}user.  Skipping...\n",  xmlTextReaderConstLocalName(reader));
        }
        else {
          printf("unknown child element {%s}%s for type {}user. Skipping...\n", xmlTextReaderConstNamespaceUri(reader), xmlTextReaderConstLocalName(reader));
        }
#endif
        status = xmlTextReaderSkipElement(reader);
      }
    }
  }

  return _user;
}

/**
 * Writes a User to XML.
 *
 * @param writer The XML writer.
 * @param _user The User to write.
 * @return The total bytes written, or -1 on error;
 */
static int xmlTextWriterWriteNs1UserType(xmlTextWriterPtr writer, struct task_manager_server_ns1_user *_user) {
  int status, totalBytes = 0, i;
  xmlChar *binaryData;
  if (_user->id != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "id", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}id...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->id));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}id. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->login != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "login", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}login...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->login));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}login. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->passwordHash != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "passwordHash", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}passwordHash...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->passwordHash));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}passwordHash. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->email != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "email", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}email...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->email));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}email. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->fistName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "fistName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}fistName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->fistName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}fistName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->lastName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "lastName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}lastName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->lastName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}lastName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->middleName != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "middleName", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}string for element {}middleName...\n");
#endif
    status = xmlTextWriterWriteXsStringType(writer, (_user->middleName));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}string for element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}middleName. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->role != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "role", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {}role for element {}role...\n");
#endif
    status = xmlTextWriterWriteNs1RoleType(writer, (_user->role));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {}role for element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}role. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }
  if (_user->locked != NULL) {
    status = xmlTextWriterStartElementNS(writer, NULL, BAD_CAST "locked", NULL);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write start element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
#if DEBUG_ENUNCIATE > 1
    printf("writing type {http://www.w3.org/2001/XMLSchema}boolean for element {}locked...\n");
#endif
    status = xmlTextWriterWriteXsBooleanType(writer, (_user->locked));
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write type {http://www.w3.org/2001/XMLSchema}boolean for element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;

    status = xmlTextWriterEndElement(writer);
    if (status < 0) {
#if DEBUG_ENUNCIATE
      printf("Failed to write end element {}locked. status: %i\n", status);
#endif
      return status;
    }
    totalBytes += status;
  }

  return totalBytes;
}

/**
 * Frees the elements of a User.
 *
 * @param _user The User to free.
 */
static void freeNs1UserType(struct task_manager_server_ns1_user *_user) {
  int i;
  if (_user->id != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor id of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->id);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor id of type task_manager_server_ns1_user...\n");
#endif
    free(_user->id);
  }
  if (_user->login != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor login of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->login);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor login of type task_manager_server_ns1_user...\n");
#endif
    free(_user->login);
  }
  if (_user->passwordHash != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor passwordHash of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->passwordHash);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor passwordHash of type task_manager_server_ns1_user...\n");
#endif
    free(_user->passwordHash);
  }
  if (_user->email != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor email of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->email);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor email of type task_manager_server_ns1_user...\n");
#endif
    free(_user->email);
  }
  if (_user->fistName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor fistName of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->fistName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor fistName of type task_manager_server_ns1_user...\n");
#endif
    free(_user->fistName);
  }
  if (_user->lastName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor lastName of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->lastName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor lastName of type task_manager_server_ns1_user...\n");
#endif
    free(_user->lastName);
  }
  if (_user->middleName != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor middleName of type task_manager_server_ns1_user...\n");
#endif
    freeXsStringType(_user->middleName);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor middleName of type task_manager_server_ns1_user...\n");
#endif
    free(_user->middleName);
  }
  if (_user->role != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor role of type task_manager_server_ns1_user...\n");
#endif
    freeNs1RoleType(_user->role);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor role of type task_manager_server_ns1_user...\n");
#endif
    free(_user->role);
  }
  if (_user->locked != NULL) {
#if DEBUG_ENUNCIATE > 1
    printf("Freeing type of accessor locked of type task_manager_server_ns1_user...\n");
#endif
    freeXsBooleanType(_user->locked);
#if DEBUG_ENUNCIATE > 1
    printf("Freeing accessor locked of type task_manager_server_ns1_user...\n");
#endif
    free(_user->locked);
  }
}
#endif /* DEF_task_manager_server_ns1_user_M */
