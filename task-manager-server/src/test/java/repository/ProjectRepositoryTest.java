package repository;

import marker.UnitCategory;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.IProjectRepository;
import ru.amster.tm.entity.Project;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.ProjectRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepositoryTest {

    private List<Project> projects = new ArrayList<>();

    private final IProjectRepository projectRepository = new ProjectRepository();

    @Before
    public void addProjectData() {
        Project project = new Project();
        Project project1 = new Project();
        Project project2 = new Project();
        Project project3 = new Project();
        project.setUserId("1");
        project.setId("1");
        project.setDescription("1");
        project.setName("1");

        project1.setUserId("1");
        project1.setId("2");
        project1.setDescription("2");
        project1.setName("2");

        project2.setUserId("2");
        project2.setId("3");
        project2.setDescription("2");
        project2.setName("2");

        project3.setUserId("3");
        project3.setId("4");
        project3.setDescription("1");
        project3.setName("1");
        projectRepository.merge(project);
        projectRepository.merge(project1);
        projectRepository.merge(project2);
        projectRepository.merge(project3);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        projects = projectRepository.findAll("1");
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
        projects = projectRepository.findAll("2");
        Assert.assertNotNull(projects);
        Assert.assertEquals(1, projects.size());
        projects = projectRepository.findAll("5");
        Assert.assertEquals(0, projects.size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findAllTestException() {
        Assert.assertNotNull(projectRepository.findAll(null));
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findAllTestException1() {
        Assert.assertNotNull(projectRepository.findAll(""));
    }


    @Test
    @Category(UnitCategory.class)
    public void removeAllTest() {
        projectRepository.removeAll("1");
        projectRepository.removeAll("2");
        projectRepository.removeAll("3");
        Assert.assertEquals(0, projects.size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeAllTestException() {
        projectRepository.removeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeAllTestException1() {
        projectRepository.removeAll("");
    }

}