package repository;

import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.repository.ITaskRepository;
import ru.amster.tm.entity.Task;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskRepositoryTest {

    private List<Task> Tasks = new ArrayList<>();

    private final ITaskRepository TaskRepository = new TaskRepository();

    @Before
    public void addTaskData() {
        Task Task = new Task();
        Task Task1 = new Task();
        Task Task2 = new Task();
        Task Task3 = new Task();
        Task.setUserId("1");
        Task.setId("1");
        Task.setDescription("1");
        Task.setName("1");

        Task1.setUserId("1");
        Task1.setId("2");
        Task1.setDescription("2");
        Task1.setName("2");

        Task2.setUserId("2");
        Task2.setId("3");
        Task2.setDescription("2");
        Task2.setName("2");

        Task3.setUserId("3");
        Task3.setId("4");
        Task3.setDescription("1");
        Task3.setName("1");
        TaskRepository.merge(Task);
        TaskRepository.merge(Task1);
        TaskRepository.merge(Task2);
        TaskRepository.merge(Task3);
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTest() {
        Tasks = TaskRepository.findAll("1");
        Assert.assertNotNull(Tasks);
        Assert.assertEquals(2, Tasks.size());
        Tasks = TaskRepository.findAll("2");
        Assert.assertNotNull(Tasks);
        Assert.assertEquals(1, Tasks.size());
        Tasks = TaskRepository.findAll("5");
        Assert.assertEquals(0, Tasks.size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findAllTestException() {
        Assert.assertNotNull(TaskRepository.findAll(null));
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void findAllTestException1() {
        Assert.assertNotNull(TaskRepository.findAll(""));
    }


    @Test
    @Category(UnitCategory.class)
    public void removeAllTest() {
        TaskRepository.removeAll("1");
        TaskRepository.removeAll("2");
        TaskRepository.removeAll("3");
        Assert.assertEquals(0, Tasks.size());
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeAllTestException() {
        TaskRepository.removeAll(null);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void removeAllTestException1() {
        TaskRepository.removeAll("");
    }

}