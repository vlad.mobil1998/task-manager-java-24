package service;

import marker.UnitCategory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.amster.tm.api.servise.IServiceLocator;
import ru.amster.tm.api.servise.ISessionService;
import ru.amster.tm.api.servise.IUserService;
import ru.amster.tm.enamuration.Role;
import ru.amster.tm.entity.Session;
import ru.amster.tm.entity.User;
import ru.amster.tm.exception.user.AccessDeniedException;
import ru.amster.tm.service.ServiceLocator;
import ru.amster.tm.util.HashUtil;

public class SessionServiceTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();;

    private ISessionService sessionService;

    private IUserService userService;

    @Before
    public void init() throws Exception {
        serviceLocator.getPropertyServer().init();
        sessionService = serviceLocator.getSessionService();
        userService = serviceLocator.getUserService();
        sessionService.setServiceLocator(serviceLocator);
    }

    @Test
    @Category(UnitCategory.class)
    public void signTest() {
        Session session = new Session();
        session.setUserId("1213asda12e444");
        session.setTimestamp(1561564215456465415l);
        session.setId("46546asd65");
        Session session1 = session.clone();

        sessionService.sign(session);
        sessionService.sign(session1);
        Assert.assertEquals(session.getSignature(), session1.getSignature());

        session1.setId("46546asd651");
        sessionService.sign(session1);
        Assert.assertNotEquals(session.getSignature(), session1.getSignature());

        session1.setId("46546asd65");
        session1.setTimestamp(1561564215456465416l);
        sessionService.sign(session1);
        Assert.assertNotEquals(session.getSignature(), session1.getSignature());
    }

    @Test
    @Category(UnitCategory.class)
    public void validateTest()  {
        userService.create("test", "test", "test");
        String secretSession = sessionService.open("test", "test");
        sessionService.validate(secretSession);
        sessionService.validate(secretSession, Role.USER);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestExceptionNull()  {
        sessionService.validate(null);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException()  {
        sessionService.validate("asdasdasd12e1eadadq2d1utf");
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException1() {
        userService.create("test", "test", "test");
        userService.lockUserByLogin("test");
        String secretSession = sessionService.open("test", "test");
        sessionService.validate(secretSession);
    }

    @Test(expected = AccessDeniedException.class)
    @Category(UnitCategory.class)
    public void validateTestException2() {
        userService.create("test", "test", "test");
        userService.lockUserByLogin("test");
        User user = userService.findByLogin("test");
        user.setId("");
        String secretSession = sessionService.open("test", "test");
        sessionService.validate(secretSession);
    }

    @Test
    @Category(UnitCategory.class)
    public void checkDataAccessTest() {

        User user = new User();
        User user1 = new User();

        user.setLogin("test");
        String passwordHash = HashUtil.salt("test");
        user.setPasswordHash(passwordHash);

        user1.setLogin("demo");
        String passwordHash1 = HashUtil.salt("demo");
        user1.setPasswordHash(passwordHash1);

        serviceLocator.getUserService().add(user);
        serviceLocator.getUserService().add(user1);

        Assert.assertTrue(sessionService.checkDataAccess("test", "test"));
        Assert.assertTrue(sessionService.checkDataAccess("demo", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", ""));
        Assert.assertFalse(sessionService.checkDataAccess(null, "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("t", null));
        Assert.assertFalse(sessionService.checkDataAccess("test1", "demo"));
        Assert.assertFalse(sessionService.checkDataAccess("test", "demo"));
    }

}